general_grievous_event = Creature:new {
	objectName = "@mob/creature_names:cis_magnaguard",
	mobType = MOB_NPC,
	customName = "General Grievous",
	socialGroup = "self",
	faction = "rebel",
	level = 300,
	chanceHit = 30,
	damageMin = 3450,
	damageMax = 5000,
	baseXp = 45000,
	baseHAM = 3352000,
	baseHAMmax = 3352000,
	armor = 2,
	resists = {75,75,95,95,95,95,95,95,135},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED + FACTIONAGGRO,
	diet = HERBIVORE,
	scale = 1.2,

	templates = {
		"object/mobile/ep3/general_grievous.iff"
	},
	lootGroups = {
	},

	primaryWeapon = "magnaguard_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",

	primaryAttacks = merge(lightsabermaster,pikemanmaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(general_grievous_event, "general_grievous_event")
