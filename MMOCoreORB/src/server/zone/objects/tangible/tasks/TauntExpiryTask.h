/*
 * TauntExpiryTask.h
 *
 *  Created on: 10/02/2024
 *      Author: Moryg
 */

#ifndef TAUNTEXPIRYTASK_H_
#define TAUNTEXPIRYTASK_H_

#include "server/zone/objects/creature/CreatureObject.h"

class TauntExpiryTask : public Task {
	ManagedReference<TangibleObject*> self;
	int value;
public:
	TauntExpiryTask(TangibleObject* creature) {
		self = creature;
	}

	void run() {
		Locker locker(self);
		ThreatMap* threatMap = self->getThreatMap();
    if (threatMap != nullptr) {
      threatMap->clearTaunt();
    }
	}
};

#endif /* TAUNTEXPIRYTASK_H_ */
