/*
 				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

/**
 * file ForceSerenityEvent.h
 * author Alpha
 * date 23.03.2014
 */

#ifndef FORCESERENITYEVENT_H_
#define FORCESERENITYEVENT_H_

#include "engine/engine.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/creature/PlayerCreatureTemplate.h"

class ForceSerenityEvent: public Task {
	ManagedReference<CreatureObject*> player;

public:
	ForceSerenityEvent(CreatureObject* pl) {
		player = pl;
	}

	void run() {
		try {
			int healthHealed = 0, actionHealed = 0, mindHealed = 0;
			healthHealed = player->healDamage(player, CreatureAttribute::HEALTH, 500);
			actionHealed = player->healDamage(player, CreatureAttribute::ACTION, 500);
			mindHealed = player->healDamage(player, CreatureAttribute::MIND, 500);
			StringIdChatParameter message("jedi_spam", "heal_self");
			message.setTO("damage");
			message.setDI(500);
			player->sendSystemMessage(message);
			player->playEffect("clienteffect/pl_force_resist_disease_self.cef");
			}
			catch (Exception& e) {
			player->error("unreported exception caught in ForceSerenityEvent::run");
		}
	}

};

#endif /* FORCESERENITYEVENT_H_ */
