--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

nightsister_clothing_schem_all = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "nightsister_hat_s01_schematic", weight = 1111111},
		{itemTemplate = "nightsister_hat_s02_schematic", weight = 1111111},
		{itemTemplate = "nightsister_hat_s03_schematic", weight = 1111111},
		{itemTemplate = "nightsister_dress_s01_schematic", weight = 1111112},
		{itemTemplate = "nightsister_boots_s01_schematic", weight = 1111111},
		{itemTemplate = "nightsister_shirt_s01_schematic", weight = 1111111},
		{itemTemplate = "nightsister_shirt_s02_schematic", weight = 1111111},
		{itemTemplate = "nightsister_pants_s01_schematic", weight = 1111111},
		{itemTemplate = "nightsister_pants_s02_schematic", weight = 1111111},
	}
}

addLootGroupTemplate("nightsister_clothing_schem_all", nightsister_clothing_schem_all)
