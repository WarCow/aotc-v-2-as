/*
 				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

/**
 * file StealthDelayEvent.h
 * author Polonel
 * date 10.01.2010
 */

#ifndef STEALTHDELAYEVENT_H_
#define STEALTHDELAYEVENT_H_

#include "engine/engine.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/creature/PlayerCreatureTemplate.h"

class StealthDelayEvent: public Task {
	ManagedReference<CreatureObject*> player;

public:
	StealthDelayEvent(CreatureObject* pl) {
		player = pl;
	}

	void run() {
		Locker playerLocker(player);

		PlayerObject* targetGhost = player->getPlayerObject();

		try {
			if (player->isOnline() && !targetGhost->isLoggingOut()) {
				player->removePendingTask("stealthdelayevent");

				ManagedReference<Zone*> zone = player->getZone();

				if (zone == nullptr)
					return;

				PlayerCreatureTemplate* playerTemplate = dynamic_cast<PlayerCreatureTemplate*>(player->getObjectTemplate());

				if (playerTemplate == nullptr)
					return;

				ManagedReference<ImageDesignSession*> session = player->getActiveSession(SessionFacadeType::IMAGEDESIGN).castTo<ImageDesignSession*>();

				if (session != nullptr) {
					session->sessionTimeout();
				}

				float height = player->getHeight();

				if (!player->isInvisible()) {
					player->sendSystemMessage("You are invisible to other players and creatures for the next 10 seconds.");
				} else {
					player->sendSystemMessage("You are now visible to all players and creatures.");
				}
				
				TangibleObject* tano = player->asTangibleObject();
				if (tano!= nullptr){
					Zone* newZone = tano->getZoneServer()->getZone(zone->getZoneName());
					
					if (newZone == nullptr) {
						tano->error("attempting to switch to unkown/disabled zone " + zone->getZoneName());
						return;
					}
					
					ManagedReference<SceneObject*> newParent = tano->getZoneServer()->getObject(player->getParentID());

					if (newParent != nullptr && newParent->getZone() == nullptr)
						return;
						
					if (newParent != nullptr && !newParent->isCellObject())
						newParent = nullptr;
						
					float newPositionX = player->getPositionX();
					float newPositionZ = player->getPositionZ();
					float newPositionY = player->getPositionY();
						
					if (newPositionZ == 0 && newParent == nullptr) {
						newPositionZ = newZone->getHeight(newPositionX, newPositionY);
					}
					
					
					tano->setInvisible(!tano->isInvisible());//set invisible tag
					//tano->destroyObjectFromWorld(false);//removes from zone
					
					Locker locker(newZone);

					tano->initializePosition(newPositionX, newPositionZ, newPositionY);
					tano->incrementMovementCounter();

					if (newParent != nullptr) {
						if (zone == newZone) {
							//tano->sendToOwner(true); //Causes load screen
							if (newParent->isCellObject()) {
								if(tano->isInvisible()==false){
									player->teleport(player->getWorldPositionX(), player->getWorldPositionZ(), player->getWorldPositionY(), 0);//prevents teleporting to 0 0
									tano->destroyObjectFromWorld(false);//removes from zone
									newZone->transferObject(tano, -1, false);
									player->teleport(newPositionX, newPositionZ, newPositionY, newParent->getObjectID());//sets correct position in cell
									player->playEffect("clienteffect/pl_force_resist_disease_self.cef");
									player->removePendingTask("appeardelayevent");
								}else{
									tano->destroyObjectFromWorld(false);//removes from zone
									newParent->transferObject(tano, -1, false);
									player->playEffect("clienteffect/medic_stasis.cef");
								}
							}
						}
					} else {
						if(tano->isInvisible()==false){
							tano->destroyObjectFromWorld(false);//removes from zone
							newZone->transferObject(tano, -1, false);
							player->playEffect("clienteffect/pl_force_resist_disease_self.cef");
							player->removePendingTask("appeardelayevent");
						}else{
							tano->destroyObjectFromWorld(false);//removes from zone
							newZone->transferObject(tano, -1, false);
							player->playEffect("clienteffect/medic_stasis.cef");
						}
					}
				}

				//player->switchZone(zone->getZoneName(), player->getPositionX(), player->getPositionZ(), player->getPositionY(), player->getParentID(), true);
			}

		} catch (Exception& e) {
			player->error("unreported exception caught in StealthDelayEvent::run");
		}

	}

};

#endif /* STEALTHDELAYEVENT_H_ */
