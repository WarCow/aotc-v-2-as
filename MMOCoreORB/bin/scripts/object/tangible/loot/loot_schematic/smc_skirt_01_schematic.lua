
object_tangible_loot_loot_schematic_smc_skirt_01_schematic = object_tangible_loot_loot_schematic_shared_smc_skirt_01_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_tailor_master",
	targetDraftSchematic = "object/draft_schematic/clothing/clothing_skirt_singing_mtn_01.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_smc_skirt_01_schematic, "object/tangible/loot/loot_schematic/smc_skirt_s01_schematic.iff")

