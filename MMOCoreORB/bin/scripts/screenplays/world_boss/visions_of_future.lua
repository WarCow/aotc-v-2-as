-- Original Screenplay Written by: TheTinyPebble
-- Adapted by Alpha 2023

-- Boss States
-- 1: Unshifted Anakin
-- 2: Anakin Red Saber
-- 3: Vader
-- 5: Crystals destroyed and visions defeated.

--[[TODO:
	Add start encounter mechanic
	Add theater [Not done server side]
	Add player scaling
	Add finish encounter function
		Delete data bytes on completion
]]

visionsFuture = ScreenPlay:new {
	scriptName = "visionsFuture",
	planetName = "tatooine",
	x = 6280,
	z = 34,
	y = 4025,

	bossShapes = { 
		"world_anakin_skywalker_light",
		"world_anakin_skywalker_balance",
		"world_anakin_skywalker_dark",
	},

	chokeCone = 60,
	poisonCloudRadius = 5,

	crystalRed = "object/tangible/jedi/crystal_red.iff",
	crystalBlack = "object/tangible/jedi/crystal_black.iff",
	radius = 25,
	crystalCount = 4, -- Gotta be dividable by 2
}

registerScreenPlay("visionsFuture", true)

function visionsFuture:start()
	-- Clear it out first
	self:cleanupScene()
	self:startEncounter()
end

function visionsFuture:stop()
	createEvent(30 * 1000, self.scriptName, "cleanupScene", nil, "")
	writeData("visionsFuture:bossShape:light", 0)
end

function visionsFuture:startEncounter()
	-- Clear it out first
	self:cleanupScene()
	if (self.crystalCount % 2 ~= 0) then
		printLuaError("crystalCount in visionsFuture screenplay was not dividable by 2")
	end
	
	broadcastGalaxy("all", "There is a great ripple in the force in Krayts End on the planet Tatooine.")

	local pBoss = spawnMobile(self.planetName, self.bossShapes[1], 0, self.x, self.z, self.y, math.random(1, 360), 0)
	writeData("visionsFuture:bossID", SceneObject(pBoss):getObjectID())
	writeData("visionsFuture:bossState", 1)
	createObserver(OBJECTDESTRUCTION, self.scriptName, "notifyBossDestroyed", pBoss)

	self:storeObjectID(pBoss)

	writeData("visionsFuture:bossShape:balance", 1)
	writeData("visionsFuture:bossShape:dark", 1)
	writeData("visionsFuture:bossShape:light", 1)

	local crystalSpawnPoints = self:getCircleSpawnPoints(self.x, self.y, self.radius, self.crystalCount)
	local shuffledSpawnPoints = self:shuffleTable(crystalSpawnPoints)

	for i = 1, #shuffledSpawnPoints do
		local xPos = shuffledSpawnPoints[i].x
		local yPos = shuffledSpawnPoints[i].y
		local zPos = getTerrainHeight(pBoss, xPos, yPos)
		local pCrystal
		if (i <= #crystalSpawnPoints / 2) then
			pCrystal = spawnSceneObject(self.planetName, self.crystalRed, xPos, zPos, yPos, 0, math.rad(math.random(360)))
			writeData("visionsFuture:crystalCount:red", readData("visionsFuture:crystalCount:red") + 1)
			writeStringData("visionsFuture:crystalColor" .. SceneObject(pCrystal):getObjectID(), "red")
			SceneObject(pCrystal):setCustomObjectName("Red Glowing Crystal") -- Temporary
		elseif (i <= #crystalSpawnPoints / 2 * 2) then
			pCrystal = spawnSceneObject(self.planetName, self.crystalBlack, xPos, zPos, yPos, 0, math.rad(math.random(360)))
			writeData("visionsFuture:crystalCount:black", readData("visionsFuture:crystalCount:black") + 1)
			writeStringData("visionsFuture:crystalColor" .. SceneObject(pCrystal):getObjectID(), "black")
			SceneObject(pCrystal):setCustomObjectName("Black Glowing Crystal") -- Temporary
		end
		writeData("visionsFuture:crystalID" .. i, SceneObject(pCrystal):getObjectID())
		TangibleObject(pCrystal):setMaxCondition("750000")
		createObserver(OBJECTDESTRUCTION, self.scriptName, "notifyCrystalDestroyed", pCrystal)

		self:storeObjectID(pCrystal)
	end

	--createEvent(2 * 60 * 60 * 1000, self.scriptName, "finishEncounter", nil, "")
	createEvent(5 * 1000, self.scriptName, "doHealingPulse", nil, "")
	createEvent(math.random(15, 20) * 1000, self.scriptName, "doBossMechanic", nil, "")
	self:setupMusic(pBoss)
	self:setMoodString(pBoss, "sad")
	spatialMoodChat(pBoss, "Live or die. The choice is yours.", "12", "4")
end

---- Shapeshift Mechanic
function visionsFuture:shapeshift(pBoss, num)
	if (pBoss == nil) then
		return
	end

	local count = 0
	if (num ~= nil and num > 0) then
		count = num
	end

	if (count == 15) then
		printLuaError("visionsFuture could not find a new shape to shift into")
		return
	end

	local bossState = readData("visionsFuture:bossState")
	local balanceState = readData("visionsFuture:bossShape:balance")
	local darkState = readData("visionsFuture:bossShape:dark")
	local lightState = readData("visionsFuture:bossShape:light")

	if (bossState == 1 and num ~= 1) then
		local shape = math.random(1, 2) + 1
		if (shape == 2) then
			if (balanceState == 1) then
				writeData("visionsFuture:bossState", 2)
				self:doChange(pBoss, self.bossShapes[shape], 2)
			end
		elseif (shape == 3) then
			if (darkState == 1) then
				writeData("visionsFuture:bossState", 3)
				self:doChange(pBoss, self.bossShapes[shape], 3)
			end
		end
	elseif (bossState > 1 and bossState ~= 5) then
		local newbossstate = 1
		if (balanceState == 0 and darkState == 0) then
			newbossstate = 5
		else
			newbossstate = 1
		end
		if (newbossstate ==5)then
			self:doChange(pBoss, self.bossShapes[2], newbossstate)
		else
			self:doChange(pBoss, self.bossShapes[1], newbossstate)
		end
	end
end

function visionsFuture:doChange(pMobile, template, newbossstate)
	local xPos = SceneObject(pMobile):getWorldPositionX()
	local zPos = SceneObject(pMobile):getWorldPositionZ()
	local yPos = SceneObject(pMobile):getWorldPositionY()
	
	self:destroyObject(pMobile)

	local pBoss = spawnMobile(self.planetName, template, 0, xPos, zPos, yPos, math.random(1, 360), 0)
	CreatureObject(pBoss):playEffect("clienteffect/entertainer_smoke_bomb_level_1.cef", "")
	writeData("visionsFuture:bossID", SceneObject(pBoss):getObjectID())
	createObserver(OBJECTDESTRUCTION, self.scriptName, "notifyBossDestroyed", pBoss)
	self:storeObjectID(pBoss)
	
	writeData("visionsFuture:bossState", newbossstate)
	self:setupMusic(pBoss)
	if (newbossstate == 1) then
		spatialMoodChat(pBoss, "You have learned nothing.", "95", "1") --says sadly
	end
	if (newbossstate == 2) then
		spatialMoodChat(pBoss, "I gave you a choice.", "31", "1") --says disappointed
	end
	if (newbossstate == 3) then
		spatialMoodChat(pBoss, "You lack conviction.", "15", "4") --announces coldly
	end
	if (newbossstate == 5) then
		spatialMoodChat(pBoss, "Time to die.", "15", "4") --announces coldly
	end
end

---- Boss Mechanics
function visionsFuture:doBossMechanic()
	local pBoss = getSceneObject(readData("visionsFuture:bossID"))
	local bossState = readData("visionsFuture:bossState")

	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		print("Boss Null or Dead")
		return
	end

	if (bossState == 1) then
		self:doGustMechanic(pBoss)
	elseif (bossState == 2 or bossState == 5) then
		local n = math.random(1, 2)
		if (n == 1) then
			self:doGustMechanic(pBoss)
		else
			self:doChokeMechanic(pBoss)
		end
	elseif (bossState == 3) then
		self:doChokeMechanic(pBoss)
	end
	
	local lightState = readData("visionsFuture:bossShape:light")
	if (lightState == 1) then
		createEvent(math.random(20, 25) * 1000, self.scriptName, "doBossMechanic", nil, "")
	end
end

function visionsFuture:doGustMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end
	
	spatialMoodChat(pBoss, "Stay Down!", "18", "80") --shouts confidently

	local playerTable = SceneObject(pBoss):getPlayersInRange(45)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				CreatureObject(pPlayer):sendSystemMessage("With a brief motion Anakin sends everyone flying with the force.")
				CreatureObject(pPlayer):setPosture(KNOCKEDDOWN)
			end
		end
		for j = 0, 3, 3 do
			CreatureObject(pPlayer):inflictDamage(pBoss, j, CreatureObject(pPlayer):getMaxHAM(j) * 0.1, 1)
		end
	end
end

function visionsFuture:doChokeMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end
	
	spatialMoodChat(pBoss, "Then you will die.", "24", "3") --admits cruelly

	local angleStart = SceneObject(pBoss):getDirectionAngle() - self.chokeCone / 2

	if (angleStart < 0) then
		angleStart = 360 + angleStart
	end

	local angleEnd = angleStart + self.chokeCone

	if (angleEnd > 360) then
		angleEnd = self.chokeCone - (360 - angleStart)
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(30)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		local x1 = CreatureObject(pBoss):getWorldPositionX()
		local y1 = CreatureObject(pBoss):getWorldPositionY()
		local x2 = CreatureObject(pPlayer):getWorldPositionX()
		local y2 = CreatureObject(pPlayer):getWorldPositionY()

		local angle = math.atan2(x2 - x1, y2 - y1) * 180 / math.pi

		if (angle < 0) then
			angle = 360 + angle
		end

		if (angle >= angleStart and angle <= angleEnd) then
			for j = 0, 6, 3 do
				CreatureObject(pPlayer):inflictDamage(pBoss, j, CreatureObject(pPlayer):getMaxHAM(j) * 0.2, 1)
			end
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 3000, HEALTH, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 3000, ACTION, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pBoss, BLEEDING, 3000, MIND, 10, 100, SceneObject(pBoss):getObjectID(), 0)
			CreatureObject(pPlayer):sendSystemMessage("The dark figure uses the force to choke the life from all those in his gaze.")
		end
	end
end

function visionsFuture:doCloudMechanic(pBoss)
	if (pBoss == nil or CreatureObject(pBoss):isDead()) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(40)

	if (playerTable == nil) then
		return
	end

	local shuffledPlayerTable = self:shuffleTable(playerTable)

	for i = 1, math.ceil(#shuffledPlayerTable / 5), 1 do
		local pPlayer = shuffledPlayerTable[i]
		self:createCloud(pPlayer)
	end
end

function visionsFuture:createCloud(pPlayer)
	if (pPlayer == nil) then
		return
	end

	local xPos = CreatureObject(pPlayer):getWorldPositionX()
	local yPos = CreatureObject(pPlayer):getWorldPositionY()
	local zPos = getTerrainHeight(pPlayer, xPos, yPos)

	local pSceneObject = spawnSceneObject(self.planetName, "object/static/particle/pt_miasma_of_fog_greenish.iff", xPos, zPos, yPos, 0, 0)
	createEvent(30 * 1000, self.scriptName, "destroyObject", pSceneObject, "")
	self:doCloudDamage(pSceneObject)
end

function visionsFuture:doCloudDamage(pSceneObject)
	if (pSceneObject == nil) then
		return
	end

	local playerTable = SceneObject(pSceneObject):getPlayersInRange(self.poisonCloudRadius)

	if (playerTable == nil) then
		createEvent(3 * 1000, self.scriptName, "doCloudDamage", pSceneObject, "")
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		local curDist = SceneObject(pPlayer):getDistanceTo(pSceneObject)

		if (curDist <= self.poisonCloudRadius) then
			CreatureObject(pPlayer):addDotState(pSceneObject, POISONED, 250, HEALTH, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pSceneObject, POISONED, 250, ACTION, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
			CreatureObject(pPlayer):addDotState(pSceneObject, POISONED, 250, MIND, 60, 100, SceneObject(pSceneObject):getObjectID(), 0)
		end
	end

	createEvent(3 * 1000, self.scriptName, "doCloudDamage", pSceneObject, "")
end

---- Healing Mechanic
function visionsFuture:doHealingPulse()
	local bossState = readData("visionsFuture:bossState")
	local pBoss = getSceneObject(readData("visionsFuture:bossID"))

	if (bossState == 1) then
		for i = 1, self.crystalCount do
			local pCrystal = getSceneObject(readData("visionsFuture:crystalID" .. i))
			if (pCrystal ~= nil) then
				TangibleObject(pCrystal):setConditionDamage(0)
			end
		end
	elseif (bossState == 2) then
		for i = 1, self.crystalCount do
			local pCrystal = getSceneObject(readData("visionsFuture:crystalID" .. i))
			if (pCrystal ~= nil and readStringData("visionsFuture:crystalColor" .. SceneObject(pCrystal):getObjectID()) ~= "red") then
				TangibleObject(pCrystal):setConditionDamage(0)
			end
		end
		if (readData("visionsFuture:crystalCount:red") > 0) then
			local pBoss = getSceneObject(readData("visionsFuture:bossID"))
			if (pBoss ~= nil) then
				self:healBoss(pBoss)
			end
		end
	elseif (bossState == 3) then
		for i = 1, self.crystalCount do
			local pCrystal = getSceneObject(readData("visionsFuture:crystalID" .. i))
			if (pCrystal ~= nil and readStringData("visionsFuture:crystalColor" .. SceneObject(pCrystal):getObjectID()) ~= "black") then
				TangibleObject(pCrystal):setConditionDamage(0)
			end
		end
		if (readData("visionsFuture:crystalCount:black") > 0) then
			local pBoss = getSceneObject(readData("visionsFuture:bossID"))
			if (pBoss ~= nil) then
				self:healBoss(pBoss)
			end
		end
	elseif (bossState == 5) then
		return
	end
	
	if (self:bossStateLogic(pBoss, 0.90, "visionsFuture:bossState", 1)) then
		self:shapeshift(pBoss)
	end
	
	local lightState = readData("visionsFuture:bossShape:light")
	if (lightState == 1) then
		createEvent(2 * 1000, self.scriptName, "doHealingPulse", nil, "")
	end
end

function visionsFuture:healBoss(pBoss)
	if (pBoss == nil) then
		return
	end

	for i = 0, 6, 3 do
		local maxHAM = CreatureObject(pBoss):getMaxHAM(i)
		CreatureObject(pBoss):setWounds(i, 0)
		CreatureObject(pBoss):setShockWounds(i, 0)
		CreatureObject(pBoss):setHAM(i, maxHAM)
	end
end

function visionsFuture:notifyBossDestroyed(pBoss, pPlayer)
	if (pBoss == nil) then
		return 0
	end

	local bossState = readData("visionsFuture:bossState")

	if (bossState == 2) then
		writeData("visionsFuture:bossShape:balance", 0)
		self:shapeshift(pBoss)
	elseif (bossState == 3) then
		writeData("visionsFuture:bossShape:dark", 0)
		self:shapeshift(pBoss)
	elseif (bossState == 5) then
		writeData("visionsFuture:bossShape:light", 0)
		createEvent(30 * 1000, self.scriptName, "cleanupScene", nil, "")
		createEvent(3 * 60 * 60 * 1000, self.scriptName, "start", nil, "")
		self:giveAwards(pBoss)
	end
	return 1
end

function visionsFuture:notifyCrystalDestroyed(pCrystal, pPlayer)
	if (pCrystal == nil) then
		return 0
	end
	
	local color = readStringData("visionsFuture:crystalColor" .. SceneObject(pCrystal):getObjectID())
	writeData("visionsFuture:crystalCount:" .. color, readData("visionsFuture:crystalCount:" .. color) - 1)
	self:destroyObject(pCrystal)

	local pBoss = getSceneObject(readData("visionsFuture:bossID"))
	self:shapeshift(pBoss)
	return 1
end

---- Utilities
-- Cleanup
function visionsFuture:destroyObject(pObject)
	if (pObject == nil) then
		return
	end

	if (SceneObject(pObject):isAiAgent()) then
		CreatureObject(pObject):setPvpStatusBitmask(0)
		forcePeace(pObject)
	end
	SceneObject(pObject):destroyObjectFromWorld()
end

function visionsFuture:cleanupScene()
	local spawnedObjects = readStringData("visionsFuture:spawnedObjects")
	local spawnedObjectsTable = HelperFuncs:splitString(spawnedObjects, ",")
	for i = 1, #spawnedObjectsTable, 1 do
		local pObject = getSceneObject(tonumber(spawnedObjectsTable[i]))
		if (pObject ~= nil) then
			if (SceneObject(pObject):isAiAgent()) then
				CreatureObject(pObject):setPvpStatusBitmask(0)
				forcePeace(pObject)
			end
			SceneObject(pObject):destroyObjectFromWorld()
		end
	end

	deleteStringData("visionsFuture:spawnedObjects")
	deleteData("visionsFuture:bossState")
	deleteData("visionsFuture:bossID")
	deleteData("visionsFuture:bossShape:balance")
	deleteData("visionsFuture:bossShape:dark")
	deleteData("visionsFuture:bossShape:light")
	deleteData("visionsFuture:crystalCount:red")
	deleteData("visionsFuture:crystalCount:black")
end

-- Store spawned objects
function visionsFuture:storeObjectID(pObject)
	if (pObject == nil) then
		return
	end
	local objectID = SceneObject(pObject):getObjectID()
	writeStringData("visionsFuture:spawnedObjects", readStringData("visionsFuture:spawnedObjects") .. "," .. objectID)
end

-- Fisher-Yates shuffle
function visionsFuture:shuffleTable(tInput)
	math.randomseed(os.time())
	local tReturn = {}
	for i = #tInput, 1, -1 do
		local j = math.random(i)
		tInput[i], tInput[j] = tInput[j], tInput[i]
		table.insert(tReturn, tInput[i])
	end
	return tReturn
end

-- Calculating evenly spaced points on the perimeter of a circle
function visionsFuture:getCircleSpawnPoints(centerX, centerY, radius, totalPoints)
	local pivotX, pivotY
	local spawnPoints = {}
	local theta = ((math.pi * 2) / totalPoints);

	for i = 1, totalPoints do
		local angle = theta * i
		pivotX = radius * math.cos(angle) + centerX;
		pivotY = radius * math.sin(angle) + centerY;
		table.insert(spawnPoints, {x = pivotX, y = pivotY})
	end
	return spawnPoints
end

-- Boss state logic
function visionsFuture:bossStateLogic(pBoss, percent, stateString, checkState)
	if (pBoss == nil) then
		return false
	end

	if (readData(stateString) == checkState) then
		for i = 0, 6, 3 do
			if (CreatureObject(pBoss):getHAM(i) <= CreatureObject(pBoss):getMaxHAM(i) * percent and CreatureObject(pBoss):getHAM(i) >= CreatureObject(pBoss):getMaxHAM(i) * 0.1) then
				return true
			end
		end
	end
	return false
end

function visionsFuture:setupMusic(pBoss)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(250)
	local musicTemplate = ""
	local bossState = readData("visionsFuture:bossState")
			
	if (bossState == 1) then
		musicTemplate = "sound/visions_01.snd"
	elseif (bossState == 2) then
		musicTemplate = "sound/visions_02.snd"
	elseif (bossState == 3) then
		musicTemplate = "sound/visions_03.snd"
	elseif (bossState == 5) then
		musicTemplate = "sound/visions_01.snd"
	end
	

	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]

			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
			end
		end
	end
end

function visionsFuture:giveAwards(pBoss)
	if (pBoss == nil) then
		return
	end

	local playerTable = SceneObject(pBoss):getPlayersInRange(250)
	local musicTemplate = "sound/visions_05.snd"

	if (#playerTable > 0) then
		for i = 0, #playerTable, 1 do
			local pPlayer = playerTable[i]
			
			if (pPlayer ~= nil and musicTemplate ~= nil) then
				CreatureObject(pPlayer):playMusicMessage(musicTemplate)
				
				local pGhost = CreatureObject(pPlayer):getPlayerObject()
				if (pGhost ~= nil and not PlayerObject(pGhost):hasBadge(239)) then
					PlayerObject(pGhost):awardBadge(168)
					PlayerObject(pGhost):awardBadge(239)
				end
				
				local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
				
				rngloot = 0 + getRandomNumber(0, 6)
				print (rngloot)

				if (pInventory == nil or SceneObject(pInventory):isContainerFullRecursive()) then
					print("Inventory Full")
					CreatureObject(pPlayer):sendSystemMessage("Your inventory is full so you did not receive the event reward.")
				else
					if rngloot == 0 or rngloot == 6 then
					local crystalID = createLoot(pInventory, "color_crystals", 300, true)
						local pCrystal = getSceneObject(crystalID)

						if (pCrystal == nil) then
							Logger:log("Crystal is nil. Unable to set Kun's Blood Crystal Color for Player ID: " .. SceneObject(pPlayer):getObjectID(), LT_ERROR)
							CreatureObject(pPlayer):sendSystemMessage("There was an error generating your Crystal Reward. Please see Support and screenshot this message.")
						else
							local colorCrystal = LuaLightsaberCrystalComponent(pCrystal)
							colorCrystal:setColor(39)
							colorCrystal:updateCrystal(39)
							CreatureObject(pPlayer):sendSystemMessage("A mysterious red crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
						end
					end
					if rngloot == 1 then
						local crystalID = createLoot(pInventory, "krayt_tissue_rare", 300, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious tissue appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 2 then
						local crystalID = createLoot(pInventory, "power_crystals", 300, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious crystal appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 3 then
						local crystalID = createLoot(pInventory, "fire_breathing_spider", 110, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious object appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 4 then
						local crystalID = createLoot(pInventory, "jedi_paintings", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious painting appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end
					if rngloot == 5 then
						local crystalID = createLoot(pInventory, "dc15event", 1, true)
						CreatureObject(pPlayer):sendSystemMessage("A mysterious schematic appears in your hand even as you start to forget the events you have just witnessed. You store it in your inventory.")
					end		
				end

			end
			
		end
	end
end
