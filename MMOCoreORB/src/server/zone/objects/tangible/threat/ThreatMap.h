/*
 * ThreatMap.h
 *
 *  Created on: 13/07/2010
 *      Author: victor
 */

#ifndef THREATMAP_H_
#define THREATMAP_H_

#include "engine/engine.h"
#include "server/zone/objects/tangible/threat/ThreatMapObserver.h"
#include "server/zone/objects/creature/variables/CooldownTimerMap.h"
#include "server/zone/objects/tangible/weapon/WeaponObject.h"

namespace server {
namespace zone {
namespace objects {
namespace creature {
class CreatureObject;
}
}
}
}

using namespace server::zone::objects::creature;

namespace server {
namespace zone {
namespace objects {
namespace tangible {
namespace threat {

//#define DEBUG

class ThreatMapEntry : public VectorMap<String, uint32> {
	uint64 threatValue;
	uint64 threatBitmask;
	int healAmount;
	uint32 nonAggroDamageTotal;
	Time startTime;

	static const int HEALING_AGGRO_FACTOR = 5;

public:
	ThreatMapEntry() {
		setNullValue(0);
		threatValue = 0;
		threatBitmask = 0;
		healAmount = 0;
		nonAggroDamageTotal = 0;
	}

	ThreatMapEntry(const ThreatMapEntry& e) : VectorMap<String, uint32>(e) {
		setNullValue(0);
		threatValue = e.threatValue;
		threatBitmask = e.threatBitmask;
		healAmount = e.healAmount;
		nonAggroDamageTotal = e.nonAggroDamageTotal;
		startTime = e.startTime;
	}

	ThreatMapEntry& operator=(const ThreatMapEntry& e) {
		if (this == &e)
			return *this;

		threatValue = e.threatValue;
		threatBitmask = e.threatBitmask;
		healAmount = e.healAmount;
		nonAggroDamageTotal = e.nonAggroDamageTotal;
		startTime = e.startTime;

		VectorMap<String, uint32>::operator=(e);

		return *this;
	}

	void addDamage(WeaponObject* weapon, uint32 damage);
	void addDamage(String xp, uint32 damage);

	void setThreatState(uint64 state);
	bool hasState(uint64 state);
	void clearThreatState(uint64 state);

	void addHeal(int value) {
		healAmount += value;

		addThreat(value * HEALING_AGGRO_FACTOR);
	}

	void addThreat(int value) {
		if (value < 0 && (-value) > threatValue) {
			threatValue = 0;
			return;
		}

		threatValue += value;
	}

	// Use for tagging a participant in combat, adds 1 unit of threat
	void addCombatParticipant() {
		if (threatValue < 1) {
			addThreat(1);
		}
	}

	int getHeal() {
		return healAmount;
	}

	uint64 getThreatValue() {
		return threatValue;
	}

	void setThreatValue(uint64 newValue) {
		threatValue = newValue;
	}

	uint32 getDurationSeconds() {
		Time now;
		return startTime.miliDifference(now) / 1000.0;
	}

	uint32 getDPS() {
		uint32 duration = getDurationSeconds();

		if (duration > 0) {
			return getTotalDamage() / getDurationSeconds();
		}

		return 0;
	}

	void removeAggro(int value) {
		threatValue -= value;
	}

	void clearAggro() {
		threatValue = 0;
	}

	uint32 getTotalDamage() {
		uint32 totalDamage = 0;

		for (int i = 0; i < size(); i++)
			totalDamage += elementAt(i).getValue();

		return totalDamage;
	}

	// getLootDamage excludes damage done by DOT's
	uint32 getLootDamage() {
		uint32 totalDamage = 0;

		for (int i = 0; i < size(); i++) {
			String type = elementAt(i).getKey();
			uint32 damage = elementAt(i).getValue();

			if (type == "dotDMG")
				continue;

			totalDamage += damage;
		}

		// Logger::console.info("Combined total damage = " + String::valueOf(totalDamage), true);

		return totalDamage;
	}

	void setNonAggroDamage(uint32 amount) {
		nonAggroDamageTotal = amount;
	}

	uint32 getNonAggroDamage() {
		return nonAggroDamageTotal;
	}
};

class ThreatMap : public VectorMap<ManagedReference<TangibleObject*>, ThreatMapEntry> {
public:
	/// Time between normal target evaluation
	enum { EVALUATIONCOOLDOWN = 10000 };
protected:
	ManagedWeakReference<TangibleObject*> self;
	CooldownTimerMap cooldownTimerMap;
	ManagedWeakReference<TangibleObject*> currentThreatTarget;
	ManagedWeakReference<TangibleObject*> currentTauntTarget;
	ManagedReference<ThreatMapObserver*> threatMapObserver;
	Mutex lockMutex;
	uint64 currentThreatMax;

public:
	ThreatMap(TangibleObject* me) : VectorMap<ManagedReference<TangibleObject*>, ThreatMapEntry>(1, 0) {
		self = me;
		currentThreatTarget = nullptr;
		currentThreatMax = 0;
		currentTauntTarget = nullptr;
		setNoDuplicateInsertPlan();
	}

	ThreatMap(const ThreatMap& map) : VectorMap<ManagedReference<TangibleObject*>, ThreatMapEntry>(map), lockMutex() {
		setNoDuplicateInsertPlan();
		self = map.self;
		currentThreatTarget = map.currentThreatTarget;
		threatMapObserver = map.threatMapObserver;
		cooldownTimerMap = map.cooldownTimerMap;
	}

	ThreatMap& operator=(const ThreatMap& map) {
		if (this == &map)
			return *this;

		setNoDuplicateInsertPlan();
		self = map.self;
		currentThreatTarget = map.currentThreatTarget;
		threatMapObserver = map.threatMapObserver;
		cooldownTimerMap = map.cooldownTimerMap;

		VectorMap<ManagedReference<TangibleObject*>, ThreatMapEntry>::operator=(map);

		return *this;
	}

	~ThreatMap() {
	}

	// If there is no ongoing taunt cooldown, it will force the defending creature to focus attacks on the taunter for the given duration.
	bool attemptTaunt(TangibleObject* attacker, uint64 duration = 5000, uint64 cooldown = 10000);

	void removeAll(bool forceRemoveAll = false);

	void removeObservers();

	void addDamage(TangibleObject* target, uint32 damage, String xp = "");
	void dropDamage(TangibleObject* target);

	bool setThreatState(TangibleObject* target, uint64 state, uint64 duration = 0, uint64 cooldown = 0);
	void clearThreatState(TangibleObject* target, uint64 state);

	bool hasState(uint64 state);
	bool isUniqueState(uint64 state);

	CreatureObject* getHighestDamagePlayer();
	CreatureObject* getHighestDamageGroupLeader();

	/// @brief Recalculates the current highest threat. Should not really be called externally
	/// @return pointer at current highest threat
	TangibleObject* getHighestThreatAttacker();

	/// @brief Returns the current aggro holder. Main method to determine aggro
	/// @return point at current aggro holder
	TangibleObject* getAggroHolder();

	uint32 getTotalDamage();

	void addTankDamage(TangibleObject *attacker, int rawDamage, int tauntMod);
	void addThreat(TangibleObject* target, uint64 duration = 0);
	void removeAggro(TangibleObject* target, int value);
	void clearAggro(TangibleObject* target);
	void clearTaunt();

	void addHeal(TangibleObject* target, int value);
	void addStateHeal(TangibleObject* target, int value);
private:
	void registerObserver(TangibleObject* target);
};
}
}
}
}
}

using namespace server::zone::objects::tangible::threat;

#endif /* THREATMAP_H_ */
