-- Original Screenplay Written by: Alpha 2024

theedAssault = ScreenPlay:new {
	scriptName = "theedAssault",
	planetName = "naboo",
	x = -5494,
	z = 6,
	y = 4396,

	bossShapes = { 
		"world_anakin_skywalker_emperor",
		"world_padme",
	},

	chokeCone = 60,
	poisonCloudRadius = 5,

	radius = 25,
	
	shuttleCleanup = 60 * 60, -- Automatic shuttle cleanup after 1 hours if the shuttle has not been told to lift off
}

registerScreenPlay("theedAssault", false)

function theedAssault:start()
	-- Clear it out first
	createEvent(2 * 1000, self.scriptName, "startEncounter", nil, "")
end

function theedAssault:stop()

end

function theedAssault:startEncounter()
	broadcastGalaxy("all", "A major battle has begun in the city of Theed. The palace is under attack!")

	local pBoss = spawnMobile(self.planetName, self.bossShapes[1], 0, self.x, self.z, self.y, math.random(1, 360), 0)

	local pBoss2 = spawnMobile(self.planetName, self.bossShapes[2], 0, self.x+5, self.z, self.y+5, math.random(1, 360), 0)

end
