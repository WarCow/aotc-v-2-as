NightbrotherCraterScreenPlay = ScreenPlay:new {
	numberOfActs = 1,
	screenplayName = "NightbrotherCraterScreenPlay",
	planet = "yavin4",
	mobiles = { 

	--NightbrotherTemple
	 {"nightbrother_archer", 900, -6467.18, 796.771, 1781.1, 198, 0},
	 {"nightbrother_archer", 900, -6475.43, 796.771, 1783.92, 203, 0},
	 {"nightbrother_archer", 900, -6443.24, 796.772, 1773.72, 188, 0},
	 {"nightbrother_archer", 900, -6437.45, 796.771, 1772.17, 196, 0},
	 {"nightbrother_creature_handler", 900, 0.26046, -1.89057e-07, -0.568907, 1, 620002521},
	 {"adolescent_acklay", 900, 3.96959, -2.42623e-07, 0.048457, 326, 620002521},
	 {"adolescent_acklay", 900, -4.033, -5.11411e-07, 0.199238, 30, 620002521},
	 {"adolescent_acklay", 900, -1.03552, -1.33514e-05, -12.1578, 19, 620002522},
	 {"adolescent_acklay", 900, -0.00304694, -2.07536e-07, 6.80405, 357, 620002521},
	 {"elite_nightbrother_elder", 1200, 0.304391, -8.82189e-06, -33.4002, 353, 620002526},
	 {"nightbrother_assasin", 900, -13.1434, -2.00001, -43.5727, 77, 620002527},
	 {"adolescent_acklay", 900, 4.14607, -5.99998, -35.4832, 315, 620002528},
	 {"force_kliknik", 900, -3.86159, -6.00006, -26.1588, 132, 620002528},
	 {"force_kliknik", 900, 13.1466, -6.00003, -22.9088, 251, 620002528},
	 {"force_kliknik", 900, 7.21831, -6.00003, -12.5471, 7, 620002528},
	 {"force_kliknik", 900, -12.2908, -6.00007, -18.6815, 150, 620002528},
	 {"nightbrother_creature_handler", 900, -15.6067, -6.00007, -20.9624, 75, 620002528},
	 {"elite_nightbrother_weapons_master", 900, -23.5723, -6.5272, -6.74116, 206, 620002530},
	 {"elite_nightbrother_elder", 900, 23.4147, -6.5272, -6.42652, 149, 620002529},

	--Maul
	 {"shadow_collective_fs_apprentice", 900, -0.729168, -1.6495e-07, -0.846749, 6, 620002537},
	 {"shadow_collective_fs_apprentice", 900, -8.80248, -7.20194e-08, 12.3042, 84, 620002536},
	 {"shadow_collective_fs_apprentice", 900, 10.0577, -1.94324e-07, 11.3808, 271, 620002536},
	 {"shadow_collective_fs_apprentice", 900, -11.0872, -1.34225e-05, -11.3418, 89, 620002538},
	 {"shadow_collective_disciple_of_maul", 1800, 0.00527257, -8.78472e-06, -32.955, 356, 620002542},
	 {"shadow_collective_disciple_of_maul", 1800, -24.0549, -6.28865, -7.22374, 180, 620002546},

	 {"shadow_collective_disciple_of_maul", 1800, 0.801738, -6.00005, -31.3261, 317, 620002544},
	 {"shadow_collective_disciple_of_maul", 1800, 22.3545, -6.00007, -19.7855, 263, 620002545},
	 {"shadow_collective_disciple_of_maul", 1800, 8.16538, -6.0001, -9.78232, 213, 620002548},
	 {"shadow_collective_disciple_of_maul", 1800, -20.4443, -6.00005, -19.7277, 93, 620002546},


	--Crater Edge
	 {"nightbrother_archer", 900, 5736.44, 800.66, -4561.17, 117, 0},
	 {"nightbrother_assasin", 900, 5759.41, 807.028, -4576.37, 115, 0},
	 {"nightbrother_assasin", 900, 5790.33, 813.04, -4591.48, 113, 0},
	 {"nightbrother_archer", 900, 5813.35, 811.782, -4602.21, 112, 0},
	 {"nightbrother_assasin", 900, 5850.01, 815.588, -4612.97, 99, 0},
	 {"nightbrother_archer", 900, 5878.1, 825.351, -4619.27, 81, 0},
	 {"nightbrother_assasin", 900, 5914.07, 814.366, -4608.25, 70, 0},
	 {"nightbrother_archer", 900, 5964.95, 796.025, -4591.19, 55, 0},
	 {"nightbrother_assasin", 900, 3001.78, 800.708, -4571.97, 66, 0},
	 {"nightbrother_archer", 900, 6032.13, 800.984, -4554.84, 40, 0},
	 {"nightbrother_assasin", 900, 6050.67, 802.368, -4533.56, 41, 0},
	 {"nightbrother_archer", 900, 6062.99, 808.603, -4519.43, 41, 0},
	 {"nightbrother_assasin", 900, 6085.58, 803.675, -4487.38, 26, 0},
	 {"nightbrother_archer", 900, 6108.66, 805.668, -4430.45, 9, 0},
	 {"nightbrother_assasin", 900, 6110.79, 808.103, -4417.94, 9, 0},
	 {"nightbrother_archer", 900, 6116.02, 811.121, -4350.99, 358, 0},
	 {"nightbrother_assasin", 900, 6113.8, 812.866, -4324.13, 348, 0},
	 {"nightbrother_archer", 900, 6101.21, 813.692, -4280.93, 326, 0},
	 {"nightbrother_assasin", 900, 6075.93, 817.853, -4242.18, 326, 0},
	 {"nightbrother_archer", 900, 6056.13, 814.429, -4220.06, 316, 0},
	 {"nightbrother_assasin", 900, 3004.41, 811.017, -4179.4, 301, 0},
	 {"nightbrother_archer", 900, 5946.11, 811.978, -4153.7, 274, 0},
	 {"nightbrother_assasin", 900, 5905.29, 817.828, -4140.34, 281, 0},
	 {"nightbrother_archer", 900, 5848.72, 806.584, -4148.62, 253, 0},
	 {"nightbrother_archer", 900, 5806.46, 810.798, -4157.65, 266, 0},
	 {"nightbrother_assasin", 900, 5772.85, 807.831, -4164.75, 240, 0},
	 {"nightbrother_archer", 900, 5746.57, 799.533, -4182.49, 225, 0},
	 {"nightbrother_assasin", 900, 5725.81, 797.674, -4203.31, 223, 0},
	 {"nightbrother_archer", 900, 5709.23, 793.819, -4222.61, 214, 0},
	 {"nightbrother_assasin", 900, 5682.21, 788.756, -4276.52, 187, 0},
	 {"nightbrother_archer", 900, 5672.69, 787.641, -4900.71, 204, 0},
	 {"nightbrother_assasin", 900, 5658.19, 798.321, -4340.27, 193, 0},
	 {"nightbrother_archer", 900, 5653.78, 797.314, -4361.45, 186, 0},
	 {"nightbrother_assasin", 900, 5653.85, 797.694, -4400.95, 175, 0},
	 {"nightbrother_archer", 900, 5656.59, 803.206, -4428.45, 169, 0},
	 {"nightbrother_assasin", 900, 5667.24, 804.637, -4461.07, 153, 0},
	 {"nightbrother_archer", 900, 5685.69, 808.447, -4498.78, 153, 0},
	 {"nightbrother_assasin", 900, 5698.5, 802.4, -4522.88, 151, 0},
	 {"nightbrother_archer", 900, 5715.36, 803.642, -4545.63, 112, 0},
	},


}
registerScreenPlay("NightbrotherCraterScreenPlay", true)

function NightbrotherCraterScreenPlay:start()
	if (isZoneEnabled("yavin4")) then
		self:spawnMobiles()
	end
end

function NightbrotherCraterScreenPlay:spawnMobiles()
	local mobiles = self.mobiles

	for i = 1, #mobiles do
		local mobile = mobiles[i]
		local mobiles = self.mobiles
		local pMobile = spawnMobile(self.planet, mobile[1], mobile[2], mobile[3], mobile[4], mobile[5], mobile[6], mobile[7])

		if pMobile ~= nil then
			AiAgent(pMobile):addCreatureFlag(AI_STATIC)
		end
 	end



end
